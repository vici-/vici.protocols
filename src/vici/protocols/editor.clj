(ns vici.protocols.editor)

(defprotocol Editor
  (connect [component stream-kw handler] " Can either connect to a strem and handle it (given a handle function) or pipe to another stream.
                                         If another stream is supplied:
                                         Both streams are bound until the end of the process
                                         A lock must be signaled any way.

                                         If a handler function is supplied:
                                         This will be an exclusive handler so one must disconnect in order to attach a new handler.
                                         The function must take an item and return a boolean with false meaning detach.")

  (disconnect [component stream-kw] "Disconnects from one of the streams.
                                    This is performed on component teardown or in order to replace the handler"))
