(ns vici.protocols.state)

(defprotocol State
  (set-state [this kw v])
  (watch [this id watch-fn])
  (unwatch [this id]))
