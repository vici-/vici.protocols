(ns vici.protocols.config)

(defprotocol Config
  (from [config ks] [config ks default])
  (as [config new-ns ks]
      [config new-ns ks default]))
